#!/bin/sh
# Desc: General purpose script to start/stop kiosk services
#

SCRIPTNAME=`basename $0`
HOSTNAME=`uname -n`
SMALLDATE=$(date +%Y%m%d)
FMTDATE=$(date +%Y-%m-%d,%H:%M)
LOGFILE="/tmp/${SCRIPTNAME}.kiosk_services.${SMALLDATE}.log"
PARAM_ERROR_MSG="Use ${SCRIPTNAME} -c [start|restart|stop] for browser or ${SCRIPTNAME} -d [start|restart|stop] for docker-compose"

##
# DEV local env
##
DCFILE="$HOME/ws/dockercompose/docker-compose.yml"
APP_URL="http://localhost:8080/"

##
#Ubuntu_2g VM env
#
#DCFILE="$HOME/apps/abb_kiosk/docker-compose.yml"
#APP_URL="https://localhost/"

#
# Greeting msg display 
#
greet() {

if [ -z "$1" ] 
then
	echo "[WARN] ${SCRIPTNAME} started without params: $@ at ${HOSTNAME} on ${FMTDATE}"  >> ${LOGFILE}
	echo ${PARAM_ERROR_MSG}
else 	
	echo "[S.OK] Starting ${SCRIPTNAME} with params: $@ at ${HOSTNAME} on ${FMTDATE}"  | tee -a ${LOGFILE}
	translate_params $@
fi
}


##
# Translate received params and execute the corresponding commands
##
translate_params(){
while getopts "d:c:" opt; do

case ${opt} in

d )
	case ${OPTARG} in

		'start')

			echo "Starting docker..." | tee -a ${LOGFILE}
			docker-compose -f ${DCFILE} up -d  && print_status
			;;

		'restart')

			echo "Restarting docker..." | tee -a ${LOGFILE}
			cd /home/myapp
			docker-compose stop && docker-compose rm -f
			docker-compose up -d 
			docker-compose -f ${DCFILE} down  && print_status
			;;
	
		'stop')
			echo "Stoping docker..." | tee -a ${LOGFILE}
			docker-compose -f ${DCFILE} down  && print_status
			;;

	esac
;;

c )
	case ${OPTARG} in

		'start')

			echo "Opening browser..." | tee -a ${LOGFILE}
			start_browser
			;;

		'restart')

			echo "Restarting browser..." | tee -a ${LOGFILE}
			restart_browser
			;;
	
		'stop')

			echo "Stopping browser..." | tee -a ${LOGFILE}
			stop_browser
			;;

	esac
;;

\? )

	echo ${PARAM_ERROR_MSG}
	;;

esac
done

exit 0
}

start_browser(){
  chromium-browser --app=${APP_URL} --start-fullscreen --kiosk --incognito --noerrdialogs --disable-translate --no-first-run --fast --fast-start --disable-infobars --disable-features=TranslateUI --disk-cache-dir=/dev/null  --password-store=basic &
  clear
}

restart_browser(){
	stop_browser
	start_browser
}

stop_browser(){
	pkill --oldest chromium
}


##
# Utility function to handle errors and stop script execution
##
print_status() {

if [ $? -eq 0 ]
then

  echo "[C.OK] Command executed flawlessly!"  | tee -a ${LOGFILE}
  exit 0
else
	echo "[ERROR] Command executed with errors, see logs" | tee -a ${LOGFILE} 
	exit 1
fi
}


greet $@